# StockData_scraping_plotting_4_Sveto



## Requirements & Installation

To let this script run without failure, python3.8 and pipenv for python3 must be installed.

Just clone and run this command

        pipenv install

After installation of deps, run the following

        pipenv shell

then run any one of 6 files, ex)

        Python3 1_list_TSLA_data.py

## Version History

- [ ] 1.0.0 - 2022.9.20
