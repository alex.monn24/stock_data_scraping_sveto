import requests
from bs4 import BeautifulSoup
import pandas as pd
import yfinance as yf
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def make_graph(stock_data, revenue_data, stock):
    fig = make_subplots(rows=2, cols=1, shared_xaxes=True,
                        subplot_titles=("Historical Share Price", "Historical Revenue"), vertical_spacing=.3)
    stock_data_specific = stock_data[stock_data.Date <= '2021--06-30']
    revenue_data_specific = revenue_data[revenue_data.Date <= '2021-06-30']
    fig.add_trace(go.Scatter(x=pd.to_datetime(stock_data_specific.Date, infer_datetime_format=True),
                             y=stock_data_specific.Close.astype("float"), name="Share Price"), row=1, col=1)
    fig.add_trace(go.Scatter(x=pd.to_datetime(revenue_data_specific.Date, infer_datetime_format=True),
                             y=revenue_data_specific.Revenue.astype("float"), name="Revenue"), row=2, col=1)
    fig.update_xaxes(title_text="Date", row=1, col=1)
    fig.update_xaxes(title_text="Date", row=2, col=1)
    fig.update_yaxes(title_text="Price ($US)", row=1, col=1)
    fig.update_yaxes(title_text="Revenue ($US Millions)", row=2, col=1)
    fig.update_layout(showlegend=False,
                      height=900,
                      title=stock,
                      xaxis_rangeslider_visible=True)
    fig.show()


gme = yf.Ticker('DME')
gme_data = gme.history(period='max')
gme_data.reset_index(inplace=True)

html_data = requests.get("https://www.macrotrends.net/stocks/charts/GME/gamestop/revenue")
soup = BeautifulSoup(html_data.content, "html.parser")
table = soup.find_all("tbody")[1]
table_rows = table.find_all("tr")

gme_revenue = []

for tr in table_rows:
    td = tr.find_all('td')
    row = [cell.text for cell in td]
    gme_revenue.append(row)

gme_revenue = pd.DataFrame(gme_revenue, columns=["Date", "Revenue"])
gme_revenue["Revenue"] = gme_revenue['Revenue'].str.replace(',|\$', "")
gme_revenue.dropna(inplace=True)
gme_revenue = gme_revenue[gme_revenue['Revenue'] != ""]

make_graph(gme_data, gme_revenue, 'GameStop')
