import requests
from bs4 import BeautifulSoup
import pandas as pd

html_data = requests.get("https://www.macrotrends.net/stocks/charts/TSLA/tesla/revenue")
soup = BeautifulSoup(html_data.content, "html.parser")
table = soup.find_all("tbody")[1]
table_rows = table.find_all("tr")

tesla_revenue = []

for tr in table_rows:
    td = tr.find_all('td')
    row = [cell.text for cell in td]
    tesla_revenue.append(row)

tesla_revenue = pd.DataFrame(tesla_revenue, columns=["Date", "Revenue"])
tesla_revenue["Revenue"] = tesla_revenue['Revenue'].str.replace(',|\$',"")
tesla_revenue.dropna(inplace=True)
tesla_revenue = tesla_revenue[tesla_revenue['Revenue'] != ""]
print(tesla_revenue.tail())